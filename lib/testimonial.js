"use strict";

document.addEventListener("DOMContentLoaded", function () {
    new Glide(".testimonial-carousel", {
        type: "carousel",
        perView: 1,
        autoplay: 3000, // Set the time between slides in milliseconds (3 seconds)
        animationDuration: 500 // Set the animation duration in milliseconds (0.5 seconds)
    }).mount();
});